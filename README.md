This chef repository installs my socialApp development project into a docker instance.
It uses an Amazon Ubuntu Web Instance to create and deploy on it.
You will need to have an SSH key to set encrypted to actually use this chef recipe.

This chef recipe for deployCowrie.rb role will also build the cowrie project (fork of choice) and start it up in a docker instance.

You can also build any custom  python application  and install it on a docker instace usig deployPython.rb role.