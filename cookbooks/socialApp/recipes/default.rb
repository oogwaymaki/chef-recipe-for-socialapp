#
# Cookbook Name:: socialApp
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

directory '/etc/docker' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

cookbook_file '/etc/docker/Dockerfile' do
   source 'Dockerfile'
   mode '0755'
   action :create
end

cookbook_file '/etc/docker/dump.sql' do
   source 'dump.sql'
   mode '0755'
   action :create
end

cookbook_file '/etc/docker/get-pip.py' do
   source 'get-pip.py'
   mode '0755'
   action :create
end

cookbook_file '/root/envupdatesudoers.sh' do
   source 'addagenttosudoers.sh'
   mode '0755'
   action :create
end

data = data_bag_item( 'ssh_deploy', 'deploymentsshkey','iloveethoca' )

file '/root/.ssh/id_rsa_for_deploy' do
  content "#{data['ssh_private_key']}"
  mode '0600'
end

execute "update ssh sudoers "do
  command '/root/envupdatesudoers.sh'
end

execute "run agent" do
 command 'rm -f  /tmp/ssh-agent.sock ; ssh-agent -s -a /tmp/ssh-agent.sock  > output.sh;chmod +x output.sh; ./output.sh;ssh-add /root/.ssh/id_rsa_for_deploy'
 user 'root'
 group'root'
 environment 'SSH_AUTH_SOCK' => '/tmp/ssh-agent.sock'
end

service 'docker' do
	action ['start','enable']
end


git '/root/deployment' do
   repository 'git@bitbucket.org:davegermiquet/socialapp.git'
   revision 'python3socialapp'
   action :sync
   user 'root'
   group 'root'
   environment 'SSH_AUTH_SOCK' => '/tmp/ssh-agent.sock'
end


execute 'tar up dockerFile and /root/deployment' do
	command 'tar -cvf /etc/docker/dockerfile.tar -C /root/ deployment/ -C /etc/docker/ Dockerfile dump.sql -C /etc/docker/ get-pip.py'
end

docker_image 'python' do
  tag 'latest'
    action :pull
end
docker_image 'socialpython' do
	tag 'v0.1.0'
	source '/etc/docker/dockerfile.tar'
	action :build
end

docker_container 'socialapp' do
  kill_after 30
  action :stop
end


docker_container 'socialapp' do
  container_name 'socialapp'
  action [ 'kill','stop','redeploy' ]  
  working_dir "/opt/socialapp/"
  command "python2.7 socialapp.py"
  repo 'socialpython'
  tag 'v0.1.0'
  port '5000:5000'
  host_name 'trulycanadian'
  domain_name 'net'
end
