# socialApp CHANGELOG

This file is used to list changes made in each version of the socialApp cookbook.

## 0.1.0
- [Dave Germiquet] - Initial release of socialApp
- Deploy Social App To Ubuntu Image with Docker 
- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
