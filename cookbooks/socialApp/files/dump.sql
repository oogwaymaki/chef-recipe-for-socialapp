PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE category (
	id INTEGER NOT NULL, 
	"categoryName" VARCHAR(255), 
	PRIMARY KEY (id)
);
INSERT INTO "category" VALUES(1,'Music');
INSERT INTO "category" VALUES(2,'Concert');
INSERT INTO "category" VALUES(3,'Reggae');
CREATE TABLE role (
	id INTEGER NOT NULL, 
	name VARCHAR(80), 
	description VARCHAR(255), 
	PRIMARY KEY (id), 
	UNIQUE (name)
);
INSERT INTO "role" VALUES(1,'user','Description');
INSERT INTO "role" VALUES(2,'admin','admin');
CREATE TABLE user (
	id INTEGER NOT NULL, 
	email VARCHAR(255), 
	password VARCHAR(255), 
	active BOOLEAN, 
	confirmed_at DATETIME, time_zone_id INTEGER, 
	PRIMARY KEY (id), 
	UNIQUE (email), 
	CHECK (active IN (0, 1))
);
INSERT INTO "user" VALUES(1,'davegermiquet@trulycanadian.net','password',1,NULL,1);
CREATE TABLE roles_users (
	user_id INTEGER, 
	role_id INTEGER, 
	FOREIGN KEY(user_id) REFERENCES user (id), 
	FOREIGN KEY(role_id) REFERENCES role (id)
);
INSERT INTO "roles_users" VALUES(1,1);
INSERT INTO "roles_users" VALUES(1,2);
CREATE TABLE address (
	id INTEGER NOT NULL, 
	user_id INTEGER, 
	title VARCHAR(255), 
	"streetAddress" VARCHAR(255), 
	"additionalInfo" VARCHAR(255), 
	"postalZip" VARCHAR(255), 
	city VARCHAR(255), 
	country VARCHAR(255), 
	PRIMARY KEY (id), 
	FOREIGN KEY(user_id) REFERENCES user (id)
);
INSERT INTO "address" VALUES(1,1,'Ethoca','100 Sheppard Ave','','k9k9k9','Toronto','CANADA');
CREATE TABLE category_directory (
	category_child INTEGER, 
	category_parent INTEGER, 
	FOREIGN KEY(category_child) REFERENCES category (id), 
	FOREIGN KEY(category_parent) REFERENCES category (id)
);
INSERT INTO "category_directory" VALUES(2,1);
INSERT INTO "category_directory" VALUES(3,1);
CREATE TABLE event (
	id INTEGER NOT NULL, 
	"eventName" VARCHAR(255), 
	"eventDescription" VARCHAR(255), 
	"timeOfEvent" DATETIME, 
	"datePosted" DATETIME, 
	"reservationExpiration" DATETIME, 
	address_id INTEGER, 
	category_id INTEGER, 
	user_id INTEGER, "photoUrl" VARCHAR(255), 
	PRIMARY KEY (id), 
	FOREIGN KEY(address_id) REFERENCES address (id), 
	FOREIGN KEY(category_id) REFERENCES category (id), 
	FOREIGN KEY(user_id) REFERENCES user (id)
);
CREATE TABLE migrate_version (
	repository_id VARCHAR(250) NOT NULL, 
	repository_path TEXT, 
	version INTEGER, 
	PRIMARY KEY (repository_id)
);
INSERT INTO "migrate_version" VALUES('database repository','/home/dave/src/socialapp/application/db_repository',2);
CREATE TABLE time_zones (
	id INTEGER NOT NULL, 
	time_zone VARCHAR(255), 
	PRIMARY KEY (id)
);
INSERT INTO "time_zones" VALUES(1,'America/Toronto');
COMMIT;
